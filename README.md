# STREAM ParaView Reader
This ParaView python module enables the native reading of STREAM output files.

It includes an optional python module, written in C, which will produce speedups of ~100 if installed.

A warning.. whilst everything seems to be loaded correctly, I've not done any extensive testing with it. Use it at your own risk.

# Requirements
- numpy
- paraview
- vtk (usually installed with paraview)
- python-dev (or equivalent, if using the C extension) 

# Installation

## Python Extension Module
> **DISCLAIMER** Though this extension definitely works.. my C programming is definitely a little wanting! There's probably a variety of rules/idioms I've broken. If you don't like seeing bad code then don't look.

`streamcfd.c` contains a python module written in C to offload data intensive operations into compiled code. It can offer a significant (x100) speed up. It is not required, since the ParaView plugin will automatically detect if it is present and will disable if necessary.

A `setup.py` script is included. To build the module, run:

```python
python setup.py build
```

The module can be installed system-wide with
```python
python setup.py install
```

or if you would prefer to not pollute your system python modules you can use the `--user` flag, which will install to a per-user site-packages directory. See [PEP 370](https://www.python.org/dev/peps/pep-0370/) for more information.

```
python setup.py install --user
```
## ParaView Plugin
There are four main ways to load plugins in ParaView:
 
1. **Using the GUI**: Go to `Tools` > `Manage Plugins..` and click the `Load New...` button underneath the `Local Plugins` list. Select the `stream-reader.py` file and click `Load`. 

2. **Using Environmental Variables**: `PV_PLUGIN_PATH` can be used to list a set of directories (separated by colon or semi-colon) which ParaView will search on startup to load plugins.

3. **Using the plugin file**: Plugins that are listed in the `.plugins` files on the client computer will automatically be listed in the Plugin Manager. A system wide file should exist in the same directory as the `pvserver` (very system dependant, however). 

4. ~~**Placing the plugin in a recognized location**: ParaView looks for plugins in the `$HOME/.config/ParaView/ParaView<version>/Plugins` directory.~~ This doesn't seem to work for me.

See https://www.paraview.org/Wiki/ParaView/Plugin_HowTo#Using_Plugins for more details.

# Usage
Once the plugin has loaded, opening a `blocks.dat` file should present you with a list of suitable readers. Select `STREAM Binary reader` from the list.

The properties box has a few options:
* `Time Set` A dropdown box containing the timesets found.
* `Point Arrays` A list of point arrays found. They are all enabled by default.
* `dsize`: Indicates the length of the FORTRAN record marker. Should be 1 (default) if the marker is 4 bytes or 2 if 8 bytes.
* `Double Precision`: Whether the data was written in double precision (REAL*8)

# Configuration
A base set of variable definitions are included in the `STREAM_VARS` list constant at the top of the `stream-reader.py` file. Adding or remove variables to find is achieved simply by adding to, or removing from, this list.

A similar variable definition, `STREAM_TOUT_VARS`, is used for variables output at a specific time.

Other config parameters in the file
 * `STREAM_GRID_DIR`: The directory containing the grid files (x.dat,y.dat,z.dat), relative to the selected blocks.dat.
 * `STREAM_VAR_DIR`: The directory containing the flow variables (as defined in `STREAM_VARS`), relative to the selected blocks.dat.
 * `STREAM_EXT`: The extension of the files, defaults to ".dat". Changing this may require a complete plugin reload (removal and add) since ParaView is configured to look for that extension when presenting a list of suitable readers.
 * `STREAM_MODULE_ENABLED`: Switch to manually disable the C Extension
 * `STREAM_TOUT_MASK`: The filemask used to locate directories relative to the selected blocks.dat
 * `STREAM_TIMEOUTPUT_FNAME`: The name of the file containing timeoutput meta data (typically `timeoutput.dat`)

 # Dealing with time
 The plugin can also read sets of variables output at varying times. If the plugin detects timeoutput present, it will present the available TimeSets in a dropdown box in the Properties panel. 
 Change this dropdown and click apply to load the time-set. 
 
 ## File Structure
 After reading `blocks.dat` the plugin will look for directories matching the `STREAM_TOUT_MASK` constant, relative to the one containing `blocks.dat`. Taking the default mask of `tout*`, as an example:
 
 ```
  - blocks.dat
  - x.dat
  - y.dat
  - z.dat
  - tout001
  | - uvel-0001.dat
  | - uvel-0002.dat
  | - timeoutput.dat
 ```
 The plugin looks for `timeoutput.dat` (or whatever the `STREAM_TIMEOUTPUT_FNAME` constant is set to), and reads the filenumbers and accompanying timevalues located within that. 
 The `timeoutput.dat` currently must be formatted in a specific way:
 ```
 1     0.1
 2     0.2
 3     0.3
 ...   ...
 {NT}  {Time}
 ```
 where `NT` is an integer representing the filename number and `Time` is a float representing the simulation time at which the file `NT` was produced.
 
 ## File naming convention
 The file-naming convention currently accounted for is
  - `{varname}-*.{STREAM_EXT}`
 
where `*` represents a zero padded integer string with between 1 and 10 zeros, and `{varname}` is whatever is stored as `fname` in `STREAM_TOUT_VARS`. The code will automatically attempt to detect how many zeros there are.
 
 # Issues
 Please feel free to report an issue if it doesn't work.
 