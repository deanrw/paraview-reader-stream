import numpy as np
import os
import time
import sys
import importlib

from vtkmodules.util.vtkAlgorithm import VTKPythonAlgorithmBase
from paraview.util.vtkAlgorithm import smproxy, smproperty, smdomain, smhint

""" ============================================
    The STREAM_VARS list defines the variables the script will search for.
    It is a list of dicts, each with the following structure:
    {
        "name":  (str)  The name of the variable as it will appear in paraview
        "desc":  (str)  A description (for reference, not currently used)
        "fname": (list) An array of STREAM filenames (without extension) to read in
    }
    The size of the fname list determines the dimensions of the variable.
    Vectors and Tensors are fully supported
"""

# -> Directory containing data variables (relative to blocks.dat)
STREAM_VAR_DIR = "./"

# -> Directory containing grid variables {x,y,z}.dat (relative to blocks.dat)
STREAM_GRID_DIR = "./"

# -> Extension of all stream files (without the period)
STREAM_EXT = "dat"

# -> Name of STREAM Python extension
STREAM_MODULE = "streamcfd"

# -> and whether to use it
STREAM_MODULE_ENABLED = True

# -> Timeoutput mask, directories named like this will be searched for timeoutput
STREAM_TOUT_MASK = "tout*"

# -> The filename containing a list of file indexes and time values
STREAM_TIMEOUTPUT_FNAME = "timeoutput"


# -> Primary variable list (see note above)
STREAM_VARS = [
    # --> Primary flow variables
    {   # velocity
        "name": "velocity",
        "desc": "Velocity",
        "fname": ["u", "v", "w"],
    },
    {   # pressure
        "name": "p",
        "desc": "Pressure", 
        "fname": ["p"],
    },
    # --> Scalar Transport
    {   # temperature
        "name": "tc",
        "desc": "Temperature", 
        "fname": ["tc"],
    },
    {   # con
        "name": "con",
        "desc": "Concentration", 
        "fname": ["con"],
    },
    # --> Turbulence
    {   # reynolds stress
        "name": "reynolds_stress",
        "desc": "Reynolds Stress", 
        "fname": ["uu","vv","ww","uv","vw","wu"],
    },
    {   # tke
        "name": "tke",
        "desc": "Turbulent Kinetic Energy", 
        "fname": ["tke"],
    },
    {   # ted
        "name": "ted",
        "desc": "Turbulent Dissipation Rate", 
        "fname": ["ted"],
    },
    {   # ted-npt (real epsilon)
        "name": "tedntp",
        "desc": "Turbulent Dissipation Rate (Real)", 
        "fname": ["tedntp"],
    },
    {   # twd
        "name": "twd",
        "desc": "Turbulent Specific Dissipation Rate", 
        "fname": ["twd"],
    },
    {   # vis
        "name": "vis",
        "desc": "Turbulent Viscosity", 
        "fname": ["vis"],
    },
    {   # turbulent heat flux
        "name": "turb_heatflux",
        "desc": "Turbulent Heat Flux", 
        "fname": ["uth","vth","wth"],
    },
    {   # temperature variance
        "name": "temp_var",
        "desc": "Temperature Variance", 
        "fname": ["tt"],
    },
    # --> Wall parameters
    {   # disn
        "name": "disn",
        "desc": "Wall Distance", 
        "fname": ["disn"],
    },     
    {  # wall-normal vector
        "name": "wall_normal",
        "desc": "Wall-normal vector Output", 
        "fname": ["rnnx","rnny","rnnz"],
    },     
    {   # yplus
        "name": "yplus",
        "desc": "STREAM YPlus", 
        "fname": ["yplus"],
    },     
    {   # ystar
        "name": "ystar",
        "desc": "YStar", 
        "fname": ["ystar"],
    },     
    {   # wall-shear stress
        "name": "tauw",
        "desc": "Wall Shear Stres", 
        "fname": ["tauw"],
    },
    {   # friction velocity
        "name": "utau",
        "desc": "Friction Velocity", 
        "fname": ["utau"],
    },
    # --> flow structure
    {   # q-criteron
        "name": "qcriteron",
        "desc": "Q-Criterion", 
        "fname": ["qcrit"],
    },
    # --> anisotropy
    {   # Lumley flatness parameter
        "name": "aflat",
        "desc": "Lumley Flatness Parameter", 
        "fname": ["aflat"],
    },
    {   # A2 invariant
        "name": "a2",
        "desc": "A2 stress invariant", 
        "fname": ["a2"],
    },
    {   # A3 invariant
        "name": "a3",
        "desc": "A3 stress invariant", 
        "fname": ["a3"],
    },
    # --> Electromagnetic
    {   # Magnetic field
        "name": "b",
        "desc": "Magnetic Field", 
        "fname": ["bx","by","bz"],
    },
    {   # Current density
        "name": "j",
        "desc": "Current Density", 
        "fname": ["jx","jy","jz"],
    },
    {   # Lorentz Force
        "name": "lorentz_force",
        "desc": "Lorentz Force", 
        "fname": ["flx","fly","flz"],
    },
    {   # electric potential
        "name": "electric_potential",
        "desc": "Electric Potential", 
        "fname": ["ep"],
    },
    # --> Residuals
    {   # velocity
        "name": "res_vel",
        "desc": "Velocity Residual", 
        "fname": ["res-U","res-V","res-W"],
    },
    {   # pressure correction
        "name": "res_pp",
        "desc": "Pressure correction Residual", 
        "fname": ["res-PP"],
    },
    {   # reynolds stress
        "name": "res_reynolds_stress",
        "desc": "Reynolds Stress Residual", 
        "fname": ["res-UU","res-VV","res-WW","res-UV","res-WU","res-VW"],
    },
    {   # tke
        "name": "res_tke",
        "desc": "TKE residual", 
        "fname": ["res-TKE"],
    },
    {   # ted
        "name": "res_ted",
        "desc": "TED Residual", 
        "fname": ["res-TED"],
    },
]
STREAM_TOUT_VARS = [
    # --> Primary flow variables
    {   # velocity
        "name": "velocity",
        "desc": "Velocity",
        "fname": ["uvel", "vvel", "wvel"],
    },
    {   # pressure
        "name": "p",
        "desc": "Pressure", 
        "fname": ["pres"],
    },
    {   # temperature
        "name": "tc",
        "desc": "Temperature", 
        "fname": ["temp"],
    },
    {   # tke
        "name": "tke",
        "desc": "Turbulent Kinetic Energy", 
        "fname": ["tken"],
    },
    {   # ted
        "name": "ted",
        "desc": "Turbulent Dissipation Rate", 
        "fname": ["tedn"],
    },
    {   # vis
        "name": "vis",
        "desc": "Turbulent Viscosity", 
        "fname": ["visn"],
    },
    {   # reynolds stress
        "name": "reynolds_stress",
        "desc": "Reynolds Stress", 
        "fname": ["uun","vvn","wwn","uvn","vwn","wun"],
    },
]
# try importing the module
if STREAM_MODULE_ENABLED:
    try:
        streammodule = importlib.import_module(STREAM_MODULE)
    except ImportError:
        STREAM_MODULE_ENABLED = False
        streammodule = None
        print("WARNING: Cannot find C extension {}, disabling".format(STREAM_MODULE))
else:
    streammodule = None

# profiler
def timeit(method):
    def timed(*args, **kwargs):
        start = time.time()
        result = method(*args, **kwargs)
        stop = time.time()
        print("FUNCTION: {} took {}".format(method.__name__,stop-start))
        return result
    return timed


def add_time_averaged_vars(vars):
    """ Function which adds tavg version of all vars """
    pass

def createModifiedCallback(anobject):
    import weakref
    weakref_obj = weakref.ref(anobject)
    anobject = None
    def _markmodified(*args, **kwargs):
        o = weakref_obj()
        if o is not None:
            o.Modified()
    return _markmodified


@smproxy.reader(name="STREAMReader", label="STREAM Binary Reader",
                extensions="dat", file_description="STREAM")
class STREAMReader(VTKPythonAlgorithmBase):
    """ 
    A reader than reads STREAM bindary data files. It requires the following files to be present
        - init.dat
        - blocks.dat
        - {x,y|z}.dat
    """        
    def __init__(self):
        VTKPythonAlgorithmBase.__init__(self, nInputPorts=0, nOutputPorts=1, outputType='vtkMultiBlockDataSet')

        self._filename = None
        self._ndim = 3
        self.dsize = 1
        self.idouble = True
        self._wd = None
        self.GridCache = []
        self.GridCacheFile = self._filename


        self._ext = STREAM_EXT
        self._use_streammodule = STREAM_MODULE_ENABLED
        
        
        # Variables
        self.TimeSetVariableList = []
        self.VariableList = []
        self._foundvars = []
        self._foundtoutvars = []

        self._set_wd()

        # time
        self._toutmask = STREAM_TOUT_MASK
        self._ftimeoutput = STREAM_TIMEOUTPUT_FNAME
        self._timesets = []

        # switches
        self.BlocksRead = False
        self.GeometryRead = False
        self.GeometryFixed = True   # don't reload the geometry everytime

        # working directory
        if self._filename is not None:
            self._wd = os.path.dirname(os.path.realpath(self._filename))
        
        # Array selection
        from vtkmodules.vtkCommonCore import vtkDataArraySelection, vtkIdList, vtkIdListCollection, vtkDataArrayCollection, vtkStringArray
        self.ArraySelectionList = []
        self.ArraySelectionList.append(vtkDataArraySelection())
        self._arrayselection = self.ArraySelectionList[0]
        self._arrayselection.AddObserver("ModifiedEvent", createModifiedCallback(self))
        
        # Time Set selection
        self.FileSetIndex = vtkIdListCollection()
        self.TimeSetSelection = vtkStringArray()
        self.CurrentTimeSetId = 0
        self.TimeSetIds = vtkIdList()
        self.FileSetIds = vtkIdList()

    def _set_wd(self):
        if self._filename is not None:
            self._wd = os.path.dirname(os.path.realpath(self._filename))

    

    def GetArraySelection(self, TimeSetId=None):
        """ Returns the current array selection """
        if TimeSetId is None:
            TimeSetId = self.CurrentTimeSetId

        return self.ArraySelectionList[TimeSetId]
    
    def _get_timeset_array_selection(self):
        return self.TimeSetSelectionList[0]

    @smproperty.stringvector(name="blocks.dat")
    @smdomain.filelist()
    @smhint.filechooser(extensions="dat", file_description="STREAM dat files")
    def SetFileName(self, fname):
        """ Specify filename """
        if self._filename != fname:
            self._filename = fname
            self._set_wd()
            self.Modified()
    
    @smproperty.intvector(name="dsize", default_values=1)
    def SetDSize(self, dsize):
        self.dsize = dsize
        self.Modified()

    @smproperty.xml("""
        <IntVectorProperty
            name="Double Precision"
            command="SetPrecision"
            number_of_elements="1"
            default_values="1">
            <BooleanDomain name="bool"/>
            <Documentation>
                The precision of the STREAM dat files.
            </Documentation>
         </IntVectorProperty>
    """) 
    def SetPrecision(self, precision):
        if self.idouble != precision:
            self.idouble = precision
            self.Modified()


    def SetTimeSetSelection(self, selection,temp):
        # find the id of the TimeSet
        i = self.TimeSetSelection.LookupValue(selection)
        if i == -1:
            return
        if self.CurrentTimeSetId != i:
            self.CurrentTimeSetId = i
            self.Modified()
            self.UpdateInformation()
        
    @smproperty.xml(
    """
        <StringVectorProperty   name="TimeSetInfo"
                                command="GetTimeSetSelection"
                                information_only="1"
                                si_class="vtkSIDataArrayProperty">
            <StringArrayHelper />
        </StringVectorProperty>
        <StringVectorProperty   name="TimeSets"
                                information_property="GetArrayName"
                                command="SetTimeSetSelection"
                                number_of_elements="2"
                                label="Time Sets"
                                animateable="1"
                                immediate_update="1">
            <StringListDomain name="list">
                <RequiredProperties>
                    <Property function="ArrayList" name="TimeSetInfo" />
                </RequiredProperties>
            </StringListDomain>
            <Documentation>This property lists the Time Sets available for selection</Documentation>
        </StringVectorProperty>
        <StringVectorProperty   name="PointArrayInfo"
                                command="GetPointArraySelection"
                                number_of_elements_per_command="2"
                                information_only="1"
                                si_class="vtkSIDataArraySelectionProperty">
        </StringVectorProperty>
        <StringVectorProperty   name="PointArray"
                                information_property="PointArrayInfo"
                                command="GetPointArraySelection"
                                number_of_elements_per_command="2"
                                element_types="2 0"
                                repeat_command="1"
                                label="Point Arrays"
                                immediate_update="1"
                                si_class="vtkSIDataArraySelectionProperty">
            <ArraySelectionDomain name="array_list">
                <RequiredProperties>
                    <Property function="ArrayList" name="PointArrayInfo" />
                </RequiredProperties>
            </ArraySelectionDomain>
            <Documentation>This property lists which point-centered arrays to read.</Documentation>
            <Hints>
                <SelectionInput />
            </Hints>
        </StringVectorProperty>
    """
    )
    def GetPointArraySelection(self):
        return self.ArraySelectionList[self.CurrentTimeSetId]

    def GetNumberOfPointArrays(self):
        array_selection = self.ArraySelectionList[self.CurrentTimeSetId]
        return array_selection.GetNumberOfArrays()
    
    def GetPointArrayName(self, i):
        array_selection = self.ArraySelectionList[self.CurrentTimeSetId]
        return array_selection.GetArrayName(i)

    def GetPointArrayStatus(self, i):
        array_selection = self.ArraySelectionList[self.CurrentTimeSetId]
        return array_selection.ArrayIsEnabled(i)
    
    def GetTimeSetSelection(self):
        return self.TimeSetSelection
    
        
    @smproperty.doublevector(name="TimestepValues", information_only="1", si_class="vtkSITimeStepsProperty")
    def GetTimeStepValues(self):
        return self.GetTimeValues(self.CurrentTimeSetId)
    
    def RequestInformation(self, request, inInfoVec, outInfoVec):
        import paraview.vtk as vtk

        # set the working directory
        self._set_wd()
        
        # read blocks.dat
        self._read_block()

        # determine what variables are present
        self._find_variable_arrays()
        
        # look for timeoutput
        self._find_tout()
        
        executive = self.GetExecutive()
        outInfo = outInfoVec.GetInformationObject(0)
        outInfo.Remove(executive.TIME_STEPS())
        outInfo.Remove(executive.TIME_RANGE())

        if not self.CurrentTimeSetId == 0:
            # get the time values
            TimeValues = self.GetTimeValues()
            timerange = (TimeValues.min(), TimeValues.max())

            outInfo.Set(vtk.vtkStreamingDemandDrivenPipeline.TIME_STEPS(), TimeValues, len(TimeValues))
            outInfo.Set(vtk.vtkStreamingDemandDrivenPipeline.TIME_RANGE(), timerange[0], timerange[1], 2)
        return 1
    
    def _update_time_steps(self):
        pass

    
    def SetTimeValue(self, value):
        """ Sets the current time value """
        if self.TimeValue != value:
            self.TimeValue = value
            self.Modified()

    
    def GetTimeValues(self, TimeSetId=None):
        """ Returns the TimeValues for the current timeSet"""
        if TimeSetId is None:
            TimeSetId = self.CurrentTimeSetId

        return self._timesets[TimeSetId]['time']

    def GetTimeIndexValues(self, TimeSetId=None):
        """ Returns the NT list for the current timeSet"""
        if TimeSetId is None:
            TimeSetId = self.CurrentTimeSetId

        return self._timesets[TimeSetId]['nt']

    def GetTimeSet(self, TimeSetId=None):
        """ Returns the current timeset (structured array)"""
        if TimeSetId is None:
            TimeSetId = self.CurrentTimeSetId
        
        return self._timesets[TimeSetId]
    
    def GetTimeFileIndex(self, TimeSetId, time):
        """ Given a TimeSetId and time, return the fileindex """
        # get the time values
        TimeValues = self.GetTimeValues(TimeSetId=TimeSetId)

        # get the first one that is equal to or great
        # TODO: Maybe use np.close ?
        x = np.argmax(TimeValues>=time)
        
        # get the fileset index for the current TimeSetId
        TimeIndex = self.GetTimeIndexValues(TimeSetId=TimeSetId)
        
        return TimeIndex[x]
        
    
    #@timeit 
    def RequestData(self, request, inInfo, ouInfo):
        from vtkmodules.vtkCommonCore import vtkPoints
        from vtkmodules.vtkCommonDataModel import vtkPolyData, vtkStructuredGrid, vtkMultiBlockDataSet
        
        if self._filename is None:
            raise ValueError("Filename missing")

        mb = vtkMultiBlockDataSet.GetData(ouInfo, 0)
        mb.SetNumberOfBlocks(self.nblock)
        
        executive = self.GetExecutive()
        Info = ouInfo.GetInformationObject(0)

        self._read_geom(mb)

        # determine what case we're after
        if self.CurrentTimeSetId == 0:
            self._read_vars(mb)
        else:
            # get the update time
            timevalues = self.GetTimeValues()
            if timevalues is None or len(timevalues) == 0:
                return 1
            if Info.Has(executive.UPDATE_TIME_STEP()) and len(timevalues) > 0:
                utime = Info.Get(executive.UPDATE_TIME_STEP())
                self._read_vars(mb, time=utime)

        return 1

    #@timeit
    def _read_vars(self, mb, time=None):
        """ Reads variables """
        from paraview.vtk.util import numpy_support
        
        self.SetProgressText("Reading Variables") 

        if time is None:
            variable_list = self.VariableList
        else:
            variable_list = self.TimeSetVariableList[self.CurrentTimeSetId]
            
            # find the right index 
            fileId = self.GetTimeFileIndex(self.CurrentTimeSetId, time)
        
        array_selection = self.GetPointArraySelection()
        
        # loop through the files we have
        for j, var in enumerate(variable_list):
            # open data files
            name = var["name"]
            fnames = var["fname"]
            il = var["il"]

            self.SetProgressText("Reading Variables - {}".format(name))
            
            # skip if not activated
            if not array_selection.ArrayIsEnabled(name):
                continue
            

            fdat = [None]*len(fnames)
            
            for i, fname in enumerate(fnames):
                if il[i]:
                    if time is None:
                        fpath = os.path.join(self._wd, STREAM_VAR_DIR, fname + "." + self._ext)
                    else:
                        _fpath = self._get_tout_fname(fname, fileId, var["fzero"])
                        fpath = os.path.join(self._wd, self.TimeSetSelection.GetValue(self.CurrentTimeSetId), _fpath)

                    fdat[i] = FortranFile(fpath, dsize=self.dsize, idouble=self.idouble)


            # loop over blocks
            for nb in range(self.nblock):
                
                nx,ny,nz = (self.nx[nb],self.ny[nb],self.nz[nb])
                
                # simplify for block
                itot = 1 if nx == 1 else self.iend[nb] - self.istart[nb] + 3
                jtot = 1 if ny == 1 else self.jend[nb] - self.jstart[nb] + 3
                ktot = 1 if nz == 1 else self.kend[nb] - self.kstart[nb] + 3
                nijk = nx * ny * nz

                # read
                v = np.empty(shape=(len(fnames),itot*jtot*ktot))
                for i, fname in enumerate(fnames):
                    if fdat[i] is not None:
                        v[i] = self._datconv(
                                fdat[i].read(nijk),
                                nx,ny,nz,
                                self.istart[nb], self.iend[nb],
                                self.jstart[nb], self.jend[nb],
                                self.kstart[nb], self.kend[nb]
                            )
                    else:
                        v[i] = np.zeros(itot*jtot*ktot)

                pvar = np.column_stack(v)
                
                dataArray = numpy_support.numpy_to_vtk(pvar)
                dataArray.SetName(name)
                
                # clean up
                del v
                del pvar
                
                block = mb.GetBlock(nb)
                block.GetPointData().AddArray(dataArray)
            
            self.UpdateProgress(float(j/len(variable_list)))
            
    #@timeit 
    def _read_geom(self, mb):
        import paraview.vtk as vtk
        from paraview.vtk.util import numpy_support
        self.SetProgressText("Reading Geometry") 
        
        # set the file names
        fpath = []
        for i in ["x","y","z"]:
            _fpath = os.path.join(self._wd, STREAM_GRID_DIR, i + "." + self._ext)
            fpath.append(_fpath)
        
        # setup cache
        if self.GeometryFixed:
            # re-read geometry if the filename changed
            if self.GridCacheFile != self._filename:
                self.GeometryRead = False
            
            if not self.GeometryRead:
                self.GridCache.clear()
        
        # short circuit if we don't have time-varygin geometry 
        if self.GeometryFixed and self.GeometryRead:
            for nb in range(self.nblock):
                mb.SetBlock(nb, self.GridCache[nb])
                name = "Block {:d}".format(nb+1)
                mb.GetMetaData(nb).Set(vtk.vtkCompositeDataSet.NAME(), name)

                # clear existing points
                block = mb.GetBlock(nb)
                block.GetPointData().Initialize()

            return
        
        # open the files
        fxdat = FortranFile(fpath[0], dsize=self.dsize, idouble=self.idouble)
        fydat = FortranFile(fpath[1], dsize=self.dsize, idouble=self.idouble)
        if self._ndim == 3:
            fzdat = FortranFile(fpath[2], dsize=self.dsize, idouble=self.idouble)
        else:
            fzdat = None
        
        for nb in range(self.nblock):
            itot = 1 if self.nx[nb] == 1 else self.iend[nb] - self.istart[nb] + 2
            jtot = 1 if self.ny[nb] == 1 else self.jend[nb] - self.jstart[nb] + 2
            ktot = 1 if self.nz[nb] == 1 else self.kend[nb] - self.kstart[nb] + 2

            nijkg = itot*jtot*ktot
            if fxdat:
                x = fxdat.read(nijkg)
            if fydat:
                y = fydat.read(nijkg)
            if fzdat is not None:
                z = fzdat.read(nijkg)
            else:
                z = np.zeros(nijkg)

            itot = 1 if self.nx[nb] == 1 else self.iend[nb] - self.istart[nb] + 3
            jtot = 1 if self.ny[nb] == 1 else self.jend[nb] - self.jstart[nb] + 3
            ktot = 1 if self.nz[nb] == 1 else self.kend[nb] - self.kstart[nb] + 3

            x = self._grdconv(x, itot, jtot, ktot)
            y = self._grdconv(y, itot, jtot, ktot)
            z = self._grdconv(z, itot, jtot, ktot)
            
            dims = [itot, jtot, ktot]
            grid = vtk.vtkStructuredGrid()
            grid.SetDimensions(dims)
            points = vtk.vtkPoints()
            ppoints = np.column_stack((x,y,z))
            points.SetData(numpy_support.numpy_to_vtk(ppoints))
            grid.SetPoints(points)
            mb.SetBlock(nb, grid)
            if self.GeometryFixed:
                self.GridCache.append(grid)
            else:
                del grid
            
            del points

            name = "Block {:d}".format(nb+1)
            mb.GetMetaData(nb).Set(vtk.vtkCompositeDataSet.NAME(), name)
            self.UpdateProgress(float(nb/self.nblock))
        
        self.GeometryRead = True
        if self.GeometryFixed:
            self.GridCacheFile = self._filename
        
    def _find_tout(self):
        """ Look for timeoutput files """
        import fnmatch
        import paraview.vtk as vtk
        from paraview.vtk.util import numpy_support

        run_dir = self._wd
        
        dirs = [d for d in os.listdir(run_dir) if os.path.isdir(os.path.join(run_dir,d)) and fnmatch.fnmatch(d,self._toutmask)]
        
        info = {
            'num_sets': len(dirs),
            'sets': {}
        }
        fname = self._ftimeoutput + "." + STREAM_EXT

        # TimeSetSelection (vtkStringArray)
        self.TimeSetSelection.Initialize()
        self.TimeSetSelection.SetNumberOfValues(len(dirs)+1)
        self.TimeSetSelection.InsertValue(0,"None")

        self.TimeSetIds.Initialize()
        self.TimeSetIds.SetNumberOfIds(len(dirs))
        self.TimeSetIds.InsertNextId(0)

        self.FileSetIds.Initialize()
        self.FileSetIds.InsertNextId(0)

        self.FileSetIndex.RemoveAllItems()
        self.FileSetIndex.AddItem(vtk.vtkIdList())
        
        self.TimeSetVariableList.clear() 
        self.TimeSetVariableList.append(None)

        self._timesets.clear()
        self._timesets.append(None)

        for i, d in enumerate(dirs):
            # read the timeoutput file
            # read timeoutput.dat
            fpath = os.path.join(run_dir, d, fname)
            if os.path.isfile(fpath):
                try:
                    with open(fpath, 'r') as f:
                        series = np.loadtxt(f, dtype={'names': ('nt', 'time'), 'formats': (np.int64, np.float64)})
                except Exception:
                    print("WARNING: Cannot read '{}' in TimeSet directory '{}'".format(fpath, d))
                    continue
                
                # read the timevalues
                self._timesets.append(series)
                timevalues = numpy_support.numpy_to_vtk(series['time'], array_type=vtk.VTK_FLOAT)
                #fileindex2 = numpy_support.numpy_to_vtk(series['nt'], array_type=vtk.VTK_INT)
                fileindex = vtk.vtkIdList()
                fileindex.SetNumberOfIds(len(series['nt']))
                for j in range(len(series['nt'])):
                    fileindex.InsertId(j, series['nt'][j])
                
                # add the timevalues to the timesets
                self.FileSetIndex.AddItem(fileindex)
                self.TimeSetIds.InsertNextId(i+1)
                self.FileSetIds.InsertNextId(i+1)
                del timevalues
                #del fileindex2

                arrayselection = vtk.vtkDataArraySelection()
                # search for variables
                try:
                    self._find_tout_arrays(d, i+1, arrayselection)
                except (FileNotFoundError,RuntimeError) as e:
                    print("WARNING: Timeset '{}' cannot be loaded. {}".format(d, e))
                else:
                    # make it available for selection
                    self.TimeSetSelection.InsertValue(i+1,d)
                    
                    # add the data array selection
                    self.ArraySelectionList.append(arrayselection)
                 
            else:
                print("ERROR: can't load {} in TimeSet '{}'".format(fname, d))
                continue

            info['sets'][d] = {
                'nt': series[-1][0] - series[0][0] + 1,
                'start_time' : series[0][1],
                'end_time': series[-1][1],
            }

        self._tout_info = info
    
    def _get_tout_fname(self, f, i, fzero):
        """
        Given i and the number of expected zeros,return a file name
        """
        frmstring = "{:0" + str(fzero+1) + "d}"
        istr = frmstring.format(i)
        return f + "-" + istr + "." + STREAM_EXT
    
    def _find_tout_arrays(self, toutdir, TimeSetId, arrayselection):
        """
        Find timeoutput variables (only gets extent.. does not check them all
        """
        import re
        import fnmatch
        fdir = os.path.join(self._wd, toutdir)
        
        # attemp to determine the filename format from first var fname found
        fzero = -1
        for var in STREAM_TOUT_VARS:
            fnames = var["fname"]
            izero = 1
            for i in range(izero, izero+10):
                # construct the file name
                fname = fnames[0] + "-" + "0"*i + "1" + "." + STREAM_EXT
                # test for existance
                fpath = os.path.join(fdir, fname)
                if os.path.isfile(fpath):
                    fzero = i
                    break
            else:
                # nothing found for this var
                break
        if fzero == -1:
            # nothing found at all 
            msg = "No tout files found"
            raise FileNotFoundError(msg)
        
        found_vars = []

        for var in STREAM_TOUT_VARS:
            name = var["name"]
            fnames = var["fname"]
            il = [False]*len(fnames)

            # we only check the start and the end for the given range 
            fileSetId = self.FileSetIds.GetId(TimeSetId)
            fileindex = self.FileSetIndex.GetItem(fileSetId)
            sindex = fileindex.GetId(0)
            eindex = fileindex.GetId(fileindex.GetNumberOfIds()-1)

            try:
                for j, fname in enumerate(fnames):
                    fstart = self._get_tout_fname(fname, sindex, fzero)
                    
                    if len(fnames) == 3 and (j+1 > self._ndim):
                        continue
                    elif len(fnames) == 6 and (j+1 > self._ndim*2):
                        continue
                    
                    fend = self._get_tout_fname(fname, eindex, fzero)
                    if not os.path.isfile(os.path.join(fdir, fstart)):
                        raise FileNotFoundError(fstart)
                    else:
                        il[j] = True
                    if not os.path.isfile(os.path.join(fdir, fend)):
                        il[j] = False
                        raise FileNotFoundError(fend)

            except FileNotFoundError as e:
                # the timeset doesn't contain this variable
                msg = "File not Found {}".format(str(e))
            else:
                arrayselection.AddArray(name) 
                data = var
                data["il"] = il
                data["fzero"] = fzero
                found_vars.append(var)

        self.TimeSetVariableList.append(found_vars)


    def _find_variable_arrays(self):
        """ 
        Given the directory fdir, search for known STREAM variables 
        """
        fdir = self._wd

        found_vars = []
        # loop through array dict
        for var in STREAM_VARS:
            name = var["name"]
            fnames = var["fname"]
            il = []

            for f in fnames:
                fdat = os.path.join(fdir, f + "." + self._ext)
                il.append(True if os.path.isfile(fdat) else False)

            try:
                if len(fnames) == 1:
                    # scalar
                    if not il[0]:
                        raise FileNotFoundError
                elif len(fnames) == 3:
                    # vector
                    for i in range(self._ndim):
                        if not il[i]:
                            raise FileNotFoundError
                elif len(fnames) == 6:
                    # symmetric tensor
                    for i in range(self._ndim*2):
                        if not il[i]:
                            raise FileNotFoundError(fnames[i] + "." + self._ext)
                elif len(fnames) == 9:
                    raise NotImplementedError
            
            except FileNotFoundError as e:
                # if *some* of the files were found we raise a warning
                #if il.count(True) > 0:
                    #raise RuntimeWarning
                # we ignore
                continue
            except NotImplementedError as e:
                raise RuntimeWarning(e)

            else:
                # we have everything we need
                data = var
                data["il"] = il
                found_vars.append(data)
                self._arrayselection.AddArray(name)
        
        self.VariableList = found_vars
    
    def _read_block(self):
        # test file 
        fname = self._filename
        if fname is None:
            raise RuntimeError("Filename missing")
        
        try:
            with open(fname, 'r') as f:
                nblock = f.readline()
                nblock = int(nblock)
                nx = []
                ny = []
                nz = []
                istart = [1] * nblock
                iend = [1] * nblock
                jstart = [1] * nblock
                jend = [1] * nblock
                kstart = [1] * nblock
                kend = [1] * nblock
                maxk=1
                for i in range(nblock):
                    line = f.readline().split()
                    nx.append(int(line[0]))
                    ny.append(int(line[1]))
                    nz.append(int(line[2]))
                    bconn = []
                    if nblock > 1:
                        # read the block connectivity
                        for j in range(6):
                            line = [int(x) for x in f.readline().split()]
                            bconn.append(line)
                    else:
                        bconn = [[0]] * 6

                    if nx[i] > 1:
                        istart[i] = 2
                        if bconn[0][0] != 0:
                            istart[i] = 4
                            nx[i] = nx[i] + 2
                        iend[i] = nx[i] - 1
                        if bconn[1][0] != 0:
                            nx[i] = nx[i] + 2

                    if ny[i] > 1:
                        jstart[i] = 2
                        if bconn[2][0] != 0:
                            jstart[i] = 4
                            ny[i] = ny[i] + 2
                        jend[i] = ny[i] - 1
                        if bconn[3][0] != 0:
                            ny[i] = ny[i] + 2
                    
                    if nz[i] > 1:
                        kstart[i] = 2
                        if bconn[4][0] != 0:
                            kstart[i] = 4
                            nz[i] = nz[i] + 2
                        kend[i] = nz[i] - 1
                        if bconn[5][0] != 0:
                            nz[i] = nz[i] + 2

                    if nz[i] > maxk:
                        maxk = nz[i]
                
                if maxk == 1:
                    self._ndim = 2

                self.nblock = nblock
                self.nx = nx
                self.ny = ny
                self.nz = nz
                self.istart = istart
                self.iend = iend
                self.jstart = jstart
                self.jend = jend
                self.kstart = kstart
                self.kend = kend

        except IOError:
            print("Oh No")
            raise

        return
    
    #@timeit 
    def _grdconv(self, x, nx, ny, nz):

        if streammodule is not None and self._use_streammodule:
            # we offload
            return streammodule._grdconv(x, nx, ny, nz)

        ist = 1
        jst = nx - 1
        kst = jst*(ny-1)
        jstn = nx
        kstn = jstn*ny
        xnew = np.zeros(nx*ny*nz)
        for i in range(1,nx+1,1):
            for j in range(1,ny+1,1):
                for k in range(1,nz+1,1):
                    n = i-1+(j-1)*jst+(k-1)*kst
                    n1 = i-1+(j-1)*jstn+(k-1)*kstn
                    ip = -ist if i == nx and nx != 1 else 0
                    jp = -jst if j == ny and ny != 1 else 0
                    kp = -kst if k == nz and nz != 1 else 0
                    
                    im = 0 if i == 1 else -ist
                    jm = 0 if j == 1 else -jst
                    km = 0 if k == 1 else -kst

                    np.put(xnew, n1,(x[n+ip+jp+kp]+x[n+im+jp+kp]+x[n+ip+jm+kp]+x[n+im+jm+kp]+x[n+ip+jp+km]+x[n+im+jp+km]+x[n+ip+jm+km]+x[n+im+jm+km])/8.0)
        return xnew

    #@timeit 
    def _datconv(self, v, nx, ny, nz, istart, iend, jstart, jend,
                       kstart, kend):

        if streammodule is not None and self._use_streammodule:
            # we offload!
            return streammodule._datconv(
                    v,
                    nx,ny,nz,
                    istart, iend,
                    jstart, jend,
                    kstart, kend
                )

        istep = 1
        jstep = nx
        kstep = nx*ny
        istepn = 1
        jstepn = iend - istart + 3
        kstepn = jstepn*(jend-jstart + 3)

        ioff = 1 if nx == 1 else istart - 1
        joff = 1 if ny == 1 else jstart - 1
        koff = 1 if nz == 1 else kstart - 1
        vnew = np.zeros(nx*ny*nz)

        ist = istart - 1 if istart == 2    else istart
        ifn = iend + 1   if iend == nx - 1 else iend
        jst = jstart -1  if jstart == 2    else jstart
        jfn = jend + 1   if jend == ny - 1 else jend
        kst = kstart - 1 if kstart == 2    else kstart
        kfn = kend + 1   if kend == nz - 1 else kend
        

        # interior points and non-shared faces
        for i in range(ist,ifn+1):
            for j in range(jst,jfn+1):
                for k in range(kst,kfn+1):
                    ijk = i-1+(j-1)*jstep+(k-1)*kstep
                    ijkn = i-ioff+(j-joff)*jstepn+(k-koff)*kstepn
                    vnew[ijkn] = v[ijk]
        
        # shared faces
        if istart != 2 and nx != 1:
            vnew = self._datfconv(vnew, v, istart-1,istart-1,jstart,jend,kstart,kend,
                            jstep,kstep,jstepn,kstepn,ioff,joff,koff,istep,0)
        
        if iend != nx-1 and nx != 1:
            vnew = self._datfconv(vnew, v, iend+1,iend+1,jstart,jend,kstart,kend,
                            jstep,kstep,jstepn,kstepn,ioff,joff,koff,-istep,0)
        
        if jstart != 2 and ny != 1:
            vnew = self._datfconv(vnew, v, istart,iend,jstart-1,jstart-1,kstart,kend,
                            jstep,kstep,jstepn,kstepn,ioff,joff,koff,jstep,0)
        
        if jend != ny -1 and ny != 1:
            vnew = self._datfconv(vnew,v,istart,iend,jend+1,jend+1,kstart,kend,
                            jstep,kstep,jstepn,kstepn,ioff,joff,koff,-jstep,0)
  
        if kstart != 2 and nz != 1:
            vnew = self._datfconv(vnew,v,istart,iend,jstart,jend,kstart-1,kstart-1,
                            jstep,kstep,jstepn,kstepn,ioff,joff,koff,kstep,0)

        if kend != nz-1 and nz != 1:
            vnew = self._datfconv(vnew,v,istart,iend,jstart,jend,kend+1,kend+1,
                            jstep,kstep,jstepn,kstepn,ioff,joff,koff,-kstep,0)
        # shared edges 
        istep=1 
        jstep=iend-istart+3
        kstep=jstep*(jend-jstart+3)
        ixd,nxd = (1,2) if nx ==1 else (2, iend-istart+3)
        iyd,nyd = (1,2) if ny ==1 else (2, jend-jstart+3)
        izd,nzd = (1,2) if nz ==1 else (2, kend-kstart+3)

        if nx != 1 and ny != 1:
            if istart != 2 or jstart != 2:
                vnew = self._datfconv(vnew,vnew,1,1,1,1,izd,nzd-1,jstep,kstep,jstep,kstep,1,1,1,istep,jstep)

            if istart != 2 or jend != ny-1:
                vnew = self._datfconv(vnew,vnew,1,1,nyd,nyd,izd,nzd-1,jstep,kstep,jstep,kstep,1,1,1,istep,-jstep)

            if iend != nx-1 or jstart != 2:
                vnew = self._datfconv(vnew,vnew,nxd,nxd,1,1,izd,nzd-1,jstep,kstep,jstep,kstep,1,1,1,-istep,jstep)

            if iend != nx-1 or jend != ny-1:
                vnew = self._datfconv(vnew,vnew,nxd,nxd,nyd,nyd,izd,nzd-1,jstep,kstep,jstep,kstep,1,1,1,-istep,-jstep)

        if nx != 1 and nz != 1:
            if istart != 2 or kstart != 2:
                vnew = self._datfconv(vnew,vnew,1,1,iyd,nyd-1,1,1,jstep,kstep,jstep,kstep,1,1,1,istep,kstep)

            if iend != nx-1 or kstart != 2:
                vnew = self._datfconv(vnew,vnew,nxd,nxd,iyd,nyd-1,1,1,jstep,kstep,jstep,kstep,1,1,1,-istep,kstep)

            if istart != 2 or kend != nz-1:
                vnew = self._datfconv(vnew,vnew,1,1,iyd,nyd-1,nzd,nzd,jstep,kstep,jstep,kstep,1,1,1,istep,-kstep)
            
            if iend != nx-1 or kend != nz-1:
                vnew = self._datfconv(vnew,vnew,nxd,nxd,iyd,nyd-1,nzd,nzd,jstep,kstep,jstep,kstep,1,1,1,-istep,-kstep)
        
        if ny != 1 and nz != 1:
            if jstart != 2 or kstart != 2:
                vnew = self._datfconv(vnew,vnew,ixd,nxd-1,1,1,1,1,jstep,kstep,jstep,kstep,1,1,1,jstep,kstep)

            if jend != ny-1 or kstart != 2:
                vnew = self._datfconv(vnew,vnew,ixd,nxd-1,nyd,nyd,1,1,jstep,kstep,jstep,kstep,1,1,1,-jstep,kstep)
            
            if jstart != 2 or kend != nz-1:
                vnew = self._datfconv(vnew,vnew,ixd,nxd-1,1,1,nzd,nzd,jstep,kstep,jstep,kstep,1,1,1,jstep,-kstep)
            
            if jend != ny-1 or kend != nz-1:
                vnew = self._datfconv(vnew,vnew,ixd,nxd-1,nyd,nyd,nzd,nzd,jstep,kstep,jstep,kstep,1,1,1,-jstep,-kstep)
        
        # shared corners
        if (nx != 1 and ny != 1 and nz != 1):
            if (istart != 2 or jstart != 2 or kstart != 2):
                ijkn=0
                vnew[ijkn]=(vnew[ijkn+istepn]+vnew[ijkn+jstepn]+vnew[ijkn+kstepn])/3.0

            if (istart != 2 or jstart != 2 or kend != nz-1):
                ijkn=(kend-kstart+2)*kstepn
                vnew[ijkn]=(vnew[ijkn+istepn]+vnew[ijkn+jstepn]+vnew[ijkn-kstepn])/3.0

            if (istart != 2 or jend != ny-1 or kstart != 2):
                ijkn=(jend-jstart+2)*jstepn
                vnew[ijkn]=(vnew[ijkn+istepn]+vnew[ijkn-jstepn]+vnew[ijkn+kstepn])/3.0

            if (istart != 2 or jend != ny-1 or kend != nz-1):
                ijkn=(jend-jstart+2)*jstepn+(kend-kstart+2)*kstepn
                vnew[ijkn]=(vnew[ijkn+istepn]+vnew[ijkn-jstepn]+vnew[ijkn-kstepn])/3.0

            if (iend != nx-1 or jstart != 2 or kstart != 2):
                ijkn=iend-istart+2
                vnew[ijkn]=(vnew[ijkn-istepn]+vnew[ijkn+jstepn]+vnew[ijkn+kstepn])/3.0

            if (iend != nx-1 or jstart != 2 or kend != nz-1):
                ijkn=iend-istart+2+(kend-kstart+2)*kstepn
                vnew[ijkn]=(vnew[ijkn-istepn]+vnew[ijkn+jstepn]+vnew[ijkn-kstepn])/3.0

            if (iend != nx-1 or jend != ny-1 or kstart != 2):
                ijkn=iend-istart+2+(jend-jstart+2)*jstepn
                vnew[ijkn]=(vnew[ijkn-istepn]+vnew[ijkn-jstepn]+vnew[ijkn+kstepn])/3.0

            if (iend != nx-1 or jend != ny-1 or kend != nz-1):
                ijkn=iend-istart+2+(jend-jstart+2)*jstepn+(kend-kstart+2)*kstepn
                vnew[ijkn]=(vnew[ijkn-istepn]+vnew[ijkn-jstepn]+vnew[ijkn-kstepn])/3.0
        
        itot = 1 if nx == 1 else iend-istart+3
        jtot = 1 if ny == 1 else jend-jstart+3
        ktot = 1 if nz == 1 else kend-kstart+3

        vnew.resize(itot*jtot*ktot) 
        #for i in range((itot*jtot*ktot)+1):
        #    v[i] = vnew[i]

        return vnew

    def _datfconv(self, vnew, v, ist, ifn, jst, jfn, kst, kfn, jstp, kstp,
                           jstpn, kstpn, ioff, joff, koff, stp1, stp2):
        
        for i in range(ist,ifn+1):
            for j in range(jst, jfn+1):
                for k in range(kst, kfn+1):
                    n=i-1+(j-1)*jstp+(k-1)*kstp
                    n1=i-ioff+(j-joff)*jstpn+(k-koff)*kstpn
                    vnew[n1]=0.5*(v[n+stp1]+v[n+stp2])
        
        return vnew
        
class FortranFile():
    """ Class to read Fortran Files """
    def __init__(self, fname, dsize=1, idouble=True):
        # filename
        self.fname = fname

        # precision
        if idouble:
            self.dtype = 'f8'
        else:
            self.dtype = 'f4'

        # record length
        self.dsize = dsize

        self.f = open(self.fname, 'rb')

    def read(self, count=-1):
        # discard the record header
        np.fromfile(self.f, dtype='f4', count=self.dsize)
        
        # read 
        data = np.fromfile(self.f, dtype=self.dtype, count=count)
        
        # discard record header
        np.fromfile(self.f, dtype='f4', count=self.dsize)
        return data

    def close(self):
        self.f.close()


if __name__ == "__main__":
    # silence is golden
    pass
