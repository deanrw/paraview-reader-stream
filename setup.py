from distutils.core import setup, Extension
import numpy

module1 = Extension('streamcfd',
                    sources = ['streamcfd.c'],
                    include_dirs=[numpy.get_include()])

setup (name = 'StreamCFD',
       version = '1.0',
       description = "This is an add on",
       ext_modules = [module1])