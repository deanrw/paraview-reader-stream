#!/usr/bin/env pvpython

import vtk
from vtkmodules.vtkCommonDataModel import vtkDataSet
from vtkmodules.util.vtkAlgorithm import VTKPythonAlgorithmBase
from vtkmodules.numpy_interface import dataset_adapter as dsa
# Reader for STREAM in python

import numpy as np
import os
import time
import streammodule

# -> Directory containing data variables (relative to blocks.dat)
STREAM_VAR_DIR = "./"

# -> Directory containing grid variables {x,y,z}.dat (relative to blocks.dat)
STREAM_GRID_DIR = "./"

# -> Extension of all stream files (without the period)
STREAM_EXT = "dat"

STREAM_VARS = [
    # --> Primary flow variables
    {   # velocity
        "name": "velocity",
        "desc": "Velocity",
        "fname": ["u", "v", "w"],
    },
    {   # pressure
        "name": "p",
        "desc": "Pressure", 
        "fname": ["p"],
    },
    # --> Scalar Transport
    {   # temperature
        "name": "tc",
        "desc": "Temperature", 
        "fname": ["tc"],
    },
]

# profiler
def timeit(method):
    def timed(*args, **kwargs):
        start = time.time()
        result = method(*args, **kwargs)
        stop = time.time()
        print("FUNCTION: {} took {}".format(method.__name__,stop-start))
        return result
    return timed

def createModifiedCallback(anobject):
    import weakref
    weakref_obj = weakref.ref(anobject)
    anobject = None
    def _markmodified(*args, **kwargs):
        o = weakref_obj()
        if o is not None:
            o.Modified()
    return _markmodified

class Reader(object):
    def __init__(self):
        self._filename = "blocks.dat"
        self._ndim = 3
        self._timesteps = None
        self.dsize = 1
        self.idouble = True
        self._wd = None

        self._ext = STREAM_EXT
        
        self._foundvars = []

        self._set_wd()

    def _set_wd(self):
        if self._filename is not None:
            #self._wd = os.path.dirname(os.path.realpath(self._filename))
            self._wd = "./testdata3/"
    
    @timeit
    def _read_vars(self):
        import paraview.vtk as vtk
        from paraview.vtk.util import numpy_support
        
        # loop through the files we have
        for var in self._foundvars:
            # open data files
            name = var["name"]
            fnames = var["fname"]
            il = var["il"]


            fdat = [None]*len(fnames)
            
            for i, fname in enumerate(fnames):
                if il[i]:
                    fpath = os.path.join(self._wd, STREAM_VAR_DIR, fname + "." + self._ext)
                    fdat[i] = FortranFile(fpath, dsize=self.dsize, idouble=self.idouble)
            
            # loop over blocks
            var_conv = 0.0
            var_assign = 0.0
            for nb in range(self.nblock):
                
                nx,ny,nz = (self.nx[nb],self.ny[nb],self.nz[nb])
                
                # simplify for block
                itot = 1 if nx == 1 else self.iend[nb] - self.istart[nb] + 3
                jtot = 1 if ny == 1 else self.jend[nb] - self.jstart[nb] + 3
                ktot = 1 if nz == 1 else self.kend[nb] - self.kstart[nb] + 3
                nijk = nx * ny * nz

                # initialize vtkArray 
                #dataArray = vtk.vtkDoubleArray()
                #dataArray.SetNumberOfComponents(len(fnames))
                #dataArray.SetNumberOfTuples(itot*jtot*ktot)
                #dataArray.SetName(name)

                # read
                v = np.empty(shape=(len(fnames),itot*jtot*ktot))
                print(v.shape)
                start = time.time()
                for i, fname in enumerate(fnames):
                    if fdat[i] is not None:
                        #v[i] = fdat[i].read(nijk)
                        #v[i] = self._datconv(
                        #        v[i],
                        #        nx,ny,nz,
                        #        self.istart[nb], self.iend[nb],
                        #        self.jstart[nb], self.jend[nb],
                        #        self.kstart[nb], self.kend[nb],
                        #        nb
                        #    )
                        q = fdat[i].read(nijk)
                        p = streammodule._datconv(
                                q,
                                nx,ny,nz,
                                self.istart[nb], self.iend[nb],
                                self.jstart[nb], self.jend[nb],
                                self.kstart[nb], self.kend[nb]
                            )
                        print("P: ",p.shape)
                        print("Q: ",q.shape)
                    else:
                        v[i] = np.zeros(itot*jtot*ktot)
                
                stop = time.time()
                var_conv += stop-start
                print(" --> converting vars took {}".format(stop-start))
                start = time.time()
                pvar = np.column_stack(v)
                
                dataArray2 = numpy_support.numpy_to_vtk(pvar)
                print("Tuples :", dataArray2.GetNumberOfTuples())
                print("Components :", dataArray2.GetNumberOfComponents())
                
                # initialize vtkArray 
                dataArray = vtk.vtkDoubleArray()
                dataArray.SetNumberOfComponents(len(fnames))
                dataArray.SetNumberOfTuples(itot*jtot*ktot)
                                
                for n in range(0, itot*jtot*ktot):
                    p = [x[n] for x in v]
                    dataArray.InsertTuple(n,p)
                
                dataArray.SetName(name)
                
                stop = time.time()
                var_assign += stop-start
                print(" --> assigning took {}".format(stop-start))
                    
                #block = mb.GetBlock(nb)
                #block.GetPointData().AddArray(dataArray)
        
    
    @timeit 
    def _read_geom(self):
        import paraview.vtk as vtk
        from paraview.vtk.util import numpy_support
        
        # set the file names
        fpath = []
        for i in ["x","y","z"]:
            _fpath = os.path.join(self._wd, STREAM_GRID_DIR, i + "." + self._ext)
            fpath.append(_fpath)
        
        # open the files
        fxdat = FortranFile(fpath[0], dsize=self.dsize, idouble=self.idouble)
        fydat = FortranFile(fpath[1], dsize=self.dsize, idouble=self.idouble)
        if self._ndim == 3:
            fzdat = FortranFile(fpath[2], dsize=self.dsize, idouble=self.idouble)
        else:
            fzdat = None
        
        file_read = 0.0
        dat_conv = 0.0
        assign_data = 0.0
        for nb in range(self.nblock):
            print("Processing block {}".format(nb+1))
            itot = 1 if self.nx[nb] == 1 else self.iend[nb] - self.istart[nb] + 2
            jtot = 1 if self.ny[nb] == 1 else self.jend[nb] - self.jstart[nb] + 2
            ktot = 1 if self.nz[nb] == 1 else self.kend[nb] - self.kstart[nb] + 2

            nijkg = itot*jtot*ktot
            #start = time.time()
            if fxdat:
                x = fxdat.read(nijkg)
            if fydat:
                y = fydat.read(nijkg)
            if fzdat is not None:
                z = fzdat.read(nijkg)
            else:
                z = np.zeros(nijkg)
            #stop = time.time()
            #file_read += stop-start
            #print(" --> reading files took {}".format(stop-start))

            itot = 1 if self.nx[nb] == 1 else self.iend[nb] - self.istart[nb] + 3
            jtot = 1 if self.ny[nb] == 1 else self.jend[nb] - self.jstart[nb] + 3
            ktot = 1 if self.nz[nb] == 1 else self.kend[nb] - self.kstart[nb] + 3

            start = time.time()
            #x = streammodule._grdconv("Hi")
            #x1 = streammodule._grdconv(x, itot, jtot, ktot)
            #print("C:",len(x1))
            #x2 = self._grdconv(x, itot, jtot, ktot)
            #print("Py:", len(x2))
            #print("exit")
            #exit(0)
            #y = self._grdconv(y, itot, jtot, ktot)
            #z = self._grdconv(z, itot, jtot, ktot)
            #x = self._grdconv(x, itot, jtot, ktot)
            #y = self._grdconv(y, itot, jtot, ktot)
            #z = self._grdconv(z, itot, jtot, ktot)
            print(nb, itot, jtot, ktot)
            x = streammodule._grdconv(x, itot, jtot, ktot)
            y = streammodule._grdconv(y, itot, jtot, ktot)
            z = streammodule._grdconv(z, itot, jtot, ktot)
            stop = time.time()
            dat_conv += stop-start
            print(" --> converting data took {}".format(stop-start))

            start = time.time() 
            dims = [itot, jtot, ktot]
            grid = vtk.vtkStructuredGrid()
            grid.SetDimensions(dims)
            points = vtk.vtkPoints()
            nx,ny,nz = dims
            # stack
            ppoints = np.column_stack((x,y,z))
            #print(ppoints.shape)
            #print(nx*ny*nz)
            # transpose to handle vtk ordering (need to kee flatten alive)
            #flatten = ppoints.ravel()
            #print(ppoints)
            #points.SetData(numpy_support.numpy_to_vtk(flatten))
            #new_array = numpy_support.numpy_to_vtk(ppoints)
            #new_array = numpy_support.numpy_to_vtk(num_array=ppoints.flatten('F'), array_type=vtk.VTK_DOUBLE)
            #print(new_array.GetNumberOfComponents())
            points.SetData(numpy_support.numpy_to_vtk(ppoints))
            #points.SetData(numpy_support.numpy_to_vtk(num_array=ppoints.flatten('F'), array_type=vtk.VTK_DOUBLE))
            #for k in range(0, nz):
            #    koffset = k*nx*ny
            #    for j in range(0, ny):
            #        joffset = j*nx
            #        for i in range(0, nx):
            #            o = i+joffset+koffset
            #            p = [x[o],y[o],z[o]]
            #            if not np.array_equal(p, ppoints[o]):
            #                print("SHIT")
            #            #p = ppoints[o] 
            #            points.InsertPoint(o,p)
            stop = time.time()
            assign_data += stop-start
            print(" --> assignined data took {}".format(stop-start))
            
            grid.SetPoints(points)
            #mb.SetBlock(nb, grid)
            #name = "Block {:d}".format(nb+1)
            #mb.GetMetaData(nb).Set(vtk.vtkCompositeDataSet.NAME(), name)
        #print("Read files total    = {}".format(file_read))
        print("Convert data total = {}".format(dat_conv))
        print("Assign data took = {}".format(assign_data))
        #print("total accounted for = {}".format(file_read + dat_conv))

    def _read_block(self):
        # test file 
        fname = os.path.join(self._wd, self._filename)
        if fname is None:
            raise RuntimeError("Filename missing")
        
        try:
            with open(fname, 'r') as f:
                nblock = f.readline()
                nblock = int(nblock)
                nx = []
                ny = []
                nz = []
                istart = [1] * nblock
                iend = [1] * nblock
                jstart = [1] * nblock
                jend = [1] * nblock
                kstart = [1] * nblock
                kend = [1] * nblock
                maxk=1
                #bconn = [0] * 6
                for i in range(nblock):
                    line = f.readline().split()
                    nx.append(int(line[0]))
                    ny.append(int(line[1]))
                    nz.append(int(line[2]))
                    bconn = []
                    if nblock > 1:
                        # read the block connectivity
                        for j in range(6):
                            line = [int(x) for x in f.readline().split()]
                            bconn.append(line)
                    else:
                        bconn = [[0]] * 6

                    if nx[i] > 1:
                        istart[i] = 2
                        if bconn[0][0] != 0:
                            istart[i] = 4
                            nx[i] = nx[i] + 2
                        iend[i] = nx[i] - 1
                        if bconn[1][0] != 0:
                            nx[i] = nx[i] + 2

                    if ny[i] > 1:
                        jstart[i] = 2
                        if bconn[2][0] != 0:
                            jstart[i] = 4
                            ny[i] = ny[i] + 2
                        jend[i] = ny[i] - 1
                        if bconn[3][0] != 0:
                            ny[i] = ny[i] + 2
                    
                    if nz[i] > 1:
                        kstart[i] = 2
                        if bconn[4][0] != 0:
                            kstart[i] = 4
                            nz[i] = nz[i] + 2
                        kend[i] = nz[i] - 1
                        if bconn[5][0] != 0:
                            nz[i] = nz[i] + 2

                    if nz[i] > maxk:
                        maxk = nz[i]
                
                if maxk == 1:
                    self._ndim = 2

                self.nblock = nblock
                self.nx = nx
                self.ny = ny
                self.nz = nz
                self.istart = istart
                self.iend = iend
                self.jstart = jstart
                self.jend = jend
                self.kstart = kstart
                self.kend = kend

        except IOError:
            print("Oh No")
            raise

        return
    
    #@timeit 
    def _grdconv(self, x, nx, ny, nz):

        ist = 1
        jst = nx - 1
        kst = jst*(ny-1)
        jstn = nx
        kstn = jstn*ny
        start = time.time()
        xnew = np.zeros(nx*ny*nz)
        #xnew = [0]*nx*ny*nz
        stop = time.time()
        print("--> time alloc: {}".format(stop-start))
        start = time.time()
        for i in range(1,nx+1,1):
            for j in range(1,ny+1,1):
                for k in range(1,nz+1,1):
                    n = i-1+(j-1)*jst+(k-1)*kst
                    n1 = i-1+(j-1)*jstn+(k-1)*kstn
                    ip = -ist if i == nx and nx != 1 else 0
                    jp = -jst if j == ny and ny != 1 else 0
                    kp = -kst if k == nz and nz != 1 else 0
                    
                    im = 0 if i == 1 else -ist
                    jm = 0 if j == 1 else -jst
                    km = 0 if k == 1 else -kst

                    np.put(xnew, n1,(x[n+ip+jp+kp]+x[n+im+jp+kp]+x[n+ip+jm+kp]+x[n+im+jm+kp]+x[n+ip+jp+km]+x[n+im+jp+km]+x[n+ip+jm+km]+x[n+im+jm+km])/8.0)
        stop = time.time()        
        print("--> time loop: {}".format(stop-start))
        return xnew

    #@timeit 
    def _datconv(self, v, nx, ny, nz, istart, iend, jstart, jend,
                       kstart, kend, nb):
        istep = 1
        jstep = nx
        kstep = nx*ny
        istepn = 1
        jstepn = iend - istart + 3
        kstepn = jstepn*(jend-jstart + 3)


        ioff = 1 if nx == 1 else istart - 1
        joff = 1 if ny == 1 else jstart - 1
        koff = 1 if nz == 1 else kstart - 1
        vnew = [0.0]*nx*ny*nz

        ist = istart - 1 if istart == 2    else istart
        ifn = iend + 1   if iend == nx - 1 else iend
        jst = jstart -1  if jstart == 2    else jstart
        jfn = jend + 1   if jend == ny - 1 else jend
        kst = kstart - 1 if kstart == 2    else kstart
        kfn = kend + 1   if kend == nz - 1 else kend
        

        # interior points and non-shared faces
        for i in range(ist,ifn+1):
            for j in range(jst,jfn+1):
                for k in range(kst,kfn+1):
                    ijk = i-1+(j-1)*jstep+(k-1)*kstep
                    ijkn = i-ioff+(j-joff)*jstepn+(k-koff)*kstepn
                    vnew[ijkn] = v[ijk]
        
        # shared faces
        if istart != 2 and nx != 1:
            vnew = self._datfconv(vnew, v, istart-1,istart-1,jstart,jend,kstart,kend,
                            jstep,kstep,jstepn,kstepn,ioff,joff,koff,istep,0)
        
        if iend != nx-1 and nx != 1:
            vnew = self._datfconv(vnew, v, iend+1,iend+1,jstart,jend,kstart,kend,
                            jstep,kstep,jstepn,kstepn,ioff,joff,koff,-istep,0)
        
        if jstart != 2 and ny != 1:
            vnew = self._datfconv(vnew, v, istart,iend,jstart-1,jstart-1,kstart,kend,
                            jstep,kstep,jstepn,kstepn,ioff,joff,koff,jstep,0)
        
        if jend != ny -1 and ny != 1:
            vnew = self._datfconv(vnew,v,istart,iend,jend+1,jend+1,kstart,kend,
                            jstep,kstep,jstepn,kstepn,ioff,joff,koff,-jstep,0)
  
        if kstart != 2 and nz != 1:
            vnew = self._datfconv(vnew,v,istart,iend,jstart,jend,kstart-1,kstart-1,
                            jstep,kstep,jstepn,kstepn,ioff,joff,koff,kstep,0)

        if kend != nz-1 and nz != 1:
            vnew = self._datfconv(vnew,v,istart,iend,jstart,jend,kend+1,kend+1,
                            jstep,kstep,jstepn,kstepn,ioff,joff,koff,-kstep,0)
        # shared edges 
        istep=1 
        jstep=iend-istart+3
        kstep=jstep*(jend-jstart+3)
        ixd,nxd = (1,2) if nx ==1 else (2, iend-istart+3)
        iyd,nyd = (1,2) if ny ==1 else (2, jend-jstart+3)
        izd,nzd = (1,2) if nz ==1 else (2, kend-kstart+3)

        if nx != 1 and ny != 1:
            if istart != 2 or jstart != 2:
                vnew = self._datfconv(vnew,vnew,1,1,1,1,izd,nzd-1,jstep,kstep,jstep,kstep,1,1,1,istep,jstep)

            if istart != 2 or jend != ny-1:
                vnew = self._datfconv(vnew,vnew,1,1,nyd,nyd,izd,nzd-1,jstep,kstep,jstep,kstep,1,1,1,istep,-jstep)

            if iend != nx-1 or jstart != 2:
                vnew = self._datfconv(vnew,vnew,nxd,nxd,1,1,izd,nzd-1,jstep,kstep,jstep,kstep,1,1,1,-istep,jstep)

            if iend != nx-1 or jend != ny-1:
                vnew = self._datfconv(vnew,vnew,nxd,nxd,nyd,nyd,izd,nzd-1,jstep,kstep,jstep,kstep,1,1,1,-istep,-jstep)

        if nx != 1 and nz != 1:
            if istart != 2 or kstart != 2:
                vnew = self._datfconv(vnew,vnew,1,1,iyd,nyd-1,1,1,jstep,kstep,jstep,kstep,1,1,1,istep,kstep)

            if iend != nx-1 or kstart != 2:
                vnew = self._datfconv(vnew,vnew,nxd,nxd,iyd,nyd-1,1,1,jstep,kstep,jstep,kstep,1,1,1,-istep,kstep)

            if istart != 2 or kend != nz-1:
                vnew = self._datfconv(vnew,vnew,1,1,iyd,nyd-1,nzd,nzd,jstep,kstep,jstep,kstep,1,1,1,istep,-kstep)
            
            if iend != nx-1 or kend != nz-1:
                vnew = self._datfconv(vnew,vnew,nxd,nxd,iyd,nyd-1,nzd,nzd,jstep,kstep,jstep,kstep,1,1,1,-istep,-kstep)
        
        if ny != 1 and nz != 1:
            if jstart != 2 or kstart != 2:
                vnew = self._datfconv(vnew,vnew,ixd,nxd-1,1,1,1,1,jstep,kstep,jstep,kstep,1,1,1,jstep,kstep)

            if jend != ny-1 or kstart != 2:
                vnew = self._datfconv(vnew,vnew,ixd,nxd-1,nyd,nyd,1,1,jstep,kstep,jstep,kstep,1,1,1,-jstep,kstep)
            
            if jstart != 2 or kend != nz-1:
                vnew = self._datfconv(vnew,vnew,ixd,nxd-1,1,1,nzd,nzd,jstep,kstep,jstep,kstep,1,1,1,jstep,-kstep)
            
            if jend != ny-1 or kend != nz-1:
                vnew = self._datfconv(vnew,vnew,ixd,nxd-1,nyd,nyd,nzd,nzd,jstep,kstep,jstep,kstep,1,1,1,-jstep,-kstep)
        
        # shared corners
        if (nx != 1 and ny != 1 and nz != 1):
            if (istart != 2 or jstart != 2 or kstart != 2):
                ijkn=0
                vnew[ijkn]=(vnew[ijkn+istepn]+vnew[ijkn+jstepn]+vnew[ijkn+kstepn])/3.0

            if (istart != 2 or jstart != 2 or kend != nz-1):
                ijkn=(kend-kstart+2)*kstepn
                vnew[ijkn]=(vnew[ijkn+istepn]+vnew[ijkn+jstepn]+vnew[ijkn-kstepn])/3.0

            if (istart != 2 or jend != ny-1 or kstart != 2):
                ijkn=(jend-jstart+2)*jstepn
                vnew[ijkn]=(vnew[ijkn+istepn]+vnew[ijkn-jstepn]+vnew[ijkn+kstepn])/3.0

            if (istart != 2 or jend != ny-1 or kend != nz-1):
                ijkn=(jend-jstart+2)*jstepn+(kend-kstart+2)*kstepn
                vnew[ijkn]=(vnew[ijkn+istepn]+vnew[ijkn-jstepn]+vnew[ijkn-kstepn])/3.0

            if (iend != nx-1 or jstart != 2 or kstart != 2):
                ijkn=iend-istart+2
                vnew[ijkn]=(vnew[ijkn-istepn]+vnew[ijkn+jstepn]+vnew[ijkn+kstepn])/3.0

            if (iend != nx-1 or jstart != 2 or kend != nz-1):
                ijkn=iend-istart+2+(kend-kstart+2)*kstepn
                vnew[ijkn]=(vnew[ijkn-istepn]+vnew[ijkn+jstepn]+vnew[ijkn-kstepn])/3.0

            if (iend != nx-1 or jend != ny-1 or kstart != 2):
                ijkn=iend-istart+2+(jend-jstart+2)*jstepn
                vnew[ijkn]=(vnew[ijkn-istepn]+vnew[ijkn-jstepn]+vnew[ijkn+kstepn])/3.0

            if (iend != nx-1 or jend != ny-1 or kend != nz-1):
                ijkn=iend-istart+2+(jend-jstart+2)*jstepn+(kend-kstart+2)*kstepn
                vnew[ijkn]=(vnew[ijkn-istepn]+vnew[ijkn-jstepn]+vnew[ijkn-kstepn])/3.0
        
        itot = 1 if nx == 1 else iend-istart+3
        jtot = 1 if ny == 1 else jend-jstart+3
        ktot = 1 if nz == 1 else kend-kstart+3
        
        for i in range((itot*jtot*ktot)+1):
            v[i] = vnew[i]

        return v

    def _datfconv(self, vnew, v, ist, ifn, jst, jfn, kst, kfn, jstp, kstp,
                           jstpn, kstpn, ioff, joff, koff, stp1, stp2):
        
        for i in range(ist,ifn+1):
            for j in range(jst, jfn+1):
                for k in range(kst, kfn+1):
                    n=i-1+(j-1)*jstp+(k-1)*kstp
                    n1=i-ioff+(j-joff)*jstpn+(k-koff)*kstpn
                    vnew[n1]=0.5*(v[n+stp1]+v[n+stp2])
        
        return vnew
        
    def _find_variable_arrays(self):
        """ 
        Given the directory fdir, search for known STREAM variables 
        """
        fdir = self._wd

        # loop through array dict
        for var in STREAM_VARS:
            name = var["name"]
            fnames = var["fname"]
            il = []

            for f in fnames:
                fdat = os.path.join(fdir, f + "." + self._ext)
                il.append(True if os.path.isfile(fdat) else False)

            try:
                if len(fnames) == 1:
                    # scalar
                    if not il[0]:
                        raise FileNotFoundError
                elif len(fnames) == 3:
                    # vector
                    for i in range(self._ndim):
                        if not il[i]:
                            raise FileNotFoundError
                elif len(fnames) == 6:
                    # symmetric tensor
                    for i in range(self._ndim*2):
                        if not il[i]:
                            raise FileNotFoundError(fnames[i] + "." + self._ext)
                elif len(fnames) == 9:
                    raise NotImplementedError
            
            except FileNotFoundError:
                # if *some* of the files were found we raise a warning
                #if il.count(True) > 0:
                    #raise RuntimeWarning
                # we ignore
                continue
            except NotImplementedError as e:
                raise RuntimeWarning(e)

            else:
                # we have everything we need
                data = var
                data["il"] = il
                self._foundvars.append(data)
                #self._arrayselection.AddArray(name)

class FortranFile():
    """ Class to read Fortran Files """
    def __init__(self, fname, dsize=1, idouble=True):
        # filename
        self.fname = fname

        # precision
        if idouble:
            self.dtype = 'f8'
        else:
            self.dtype = 'f4'

        # record length
        self.dsize = dsize

        self.f = open(self.fname, 'rb')

    def read(self, count=-1):
        # discard the record header
        np.fromfile(self.f, dtype='f4', count=self.dsize)
        
        # read 
        data = np.fromfile(self.f, dtype=self.dtype, count=count)
        
        # discard record header
        np.fromfile(self.f, dtype='f4', count=self.dsize)
        return data

    def close(self):
        self.f.close()


if __name__ == "__main__":
    reader = Reader()
    reader._read_block()
    reader._read_geom()
    reader._find_variable_arrays()
    reader._read_vars()