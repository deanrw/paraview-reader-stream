#define PY_SSIZE_T_CLEAN
#define NPY_NO_DEPRECATED_API NPY_1_7_API_VERSION
#include <Python.h>
#include <numpy/arrayobject.h>
#include <math.h>

int datfconv(double dnew[], double d[], int ist, int ifn, int jst, 
             int jfn, int kst, int kfn, int jstp, int kstp, int jstpn, 
             int kstpn, int ioff, int joff, int koff, int stp1, int stp2)
{ 
    int i,j,k,n,n1;

    for (i=ist; i<=ifn; i++) { 
        for (j=jst; j<=jfn; j++) { 
            for (k=kst; k<=kfn; k++) { 
                n=i-1+(j-1)*jstp+(k-1)*kstp;
                n1=i-ioff+(j-joff)*jstpn+(k-koff)*kstpn;
                dnew[n1]=0.5*(d[n+stp1]+d[n+stp2]); 
            } 
        } 
    }
    return 0;
}

static PyObject *
stream_grdconv(PyObject *self, PyObject *args)
{
    PyArrayObject *xp=NULL;
    PyObject *xpnew=NULL;
    int i,j,k,n,n1;
    int ist,jst,kst,jstn,kstn;
    int im,jm,km,ip,jp,kp;
    int nx,ny,nz = 0;
    double *x, *xnew;
    
    if (!PyArg_ParseTuple(args, "Oiii", &xp, &nx, &ny, &nz))
        return NULL;

    /* Point x to the Data contained within xp */
    x = (double *) PyArray_DATA(xp);

    xnew = malloc(nx*ny*nz*sizeof(double));
    
    ist=1;
    jst=nx-1;
    kst=jst*(ny-1);
    jstn=nx;
    kstn=jstn*(ny);
    for (i=1; i<=nx; i++) {
        for (j=1; j<=ny; j++) {
            for (k=1; k<=nz; k++) { 
                n=i-1+(j-1)*jst+(k-1)*kst;
                n1=i-1+(j-1)*jstn+(k-1)*kstn;
                ip=0; jp=0; kp=0;
                if (i == nx && nx != 1) ip=-ist;
                if (j == ny && ny != 1) jp=-jst;
                if (k == nz && nz != 1) kp=-kst;
                im=-ist; jm=-jst; km=-kst;
                if (i == 1) im=0;
                if (j == 1) jm=0;
                if (k == 1) km=0;
                xnew[n1]=(x[n+ip+jp+kp]+x[n+im+jp+kp]+x[n+ip+jm+kp]+x[n+im+jm+kp]
                         +x[n+ip+jp+km]+x[n+im+jp+km]+x[n+ip+jm+km]+x[n+im+jm+km])/8.0;
            } 
        } 
    }
    /* Turn xnew into a new PyArray Object */
    npy_intp dims[1] = {nx*ny*nz};
    xpnew = PyArray_SimpleNewFromData(1, dims, NPY_DOUBLE, xnew); 

    /* tell numpy to free the data when it's finished with it */
    PyArray_ENABLEFLAGS((PyArrayObject*)xpnew, NPY_ARRAY_OWNDATA);
    
    return xpnew;
};

static PyObject *
stream_datconv(PyObject *self, PyObject *args)
{
    PyArrayObject *xp=NULL;
    PyObject *xpnew=NULL;
    int istep,jstep,kstep,istepn,jstepn,kstepn;
    int i,j,k,ijk,ijkn;
    int ist,ifn,jst,jfn,kst,kfn;
    int ixd,iyd,izd,nxd,nyd,nzd,itot,jtot,ktot,ioff,joff,koff;
    int nx,ny,nz = 0;
    int istart, iend, jstart, jend, kstart, kend = 0;

    double *d, *dnew;
    
    if (!PyArg_ParseTuple(args, "Oiiiiiiiii", &xp, &nx, &ny, &nz, 
                                        &istart, &iend, 
                                        &jstart, &jend,
                                        &kstart, &kend))
        return NULL;
    /* Point x to the Data contained within xp */
    d = (double *) PyArray_DATA(xp);
    
    /* Reassign nx, ny, nz */ 
    istep=1;
    jstep=nx;
    kstep=nx*ny;
    istepn=1;
    jstepn=iend-istart+3;
    kstepn=jstepn*(jend-jstart+3);

    if (nx == 1) { ioff = 1;} else { ioff = istart-1;}
    if (ny == 1) { joff = 1;} else { joff = jstart-1;}
    if (nz == 1) { koff = 1;} else { koff = kstart-1;}

    dnew=malloc(nx*ny*nz*sizeof(double));

    if (istart == 2) { ist=istart-1; } else { ist=istart; }
    if (iend == nx-1) { ifn=iend+1; } else { ifn=iend; }
    if (jstart == 2) { jst=jstart-1; } else { jst=jstart; }
    if (jend == ny-1) { jfn=jend+1; } else { jfn=jend; }
    if (kstart == 2) { kst=kstart-1; } else { kst=kstart; }
    if (kend == nz-1) { kfn=kend+1; } else { kfn=kend; }

/*   interior points and non-shared faces  */
    for (i=ist;i<=ifn;i++)
     { for (j=jst;j<=jfn;j++)
        { for (k=kst;k<=kfn;k++)
           { ijk=i-1+(j-1)*jstep+(k-1)*kstep;
             ijkn=i-ioff+(j-joff)*jstepn+(k-koff)*kstepn;
             dnew[ijkn]=d[ijk]; } } }

/*   shared faces  */
    if (istart != 2 && nx != 1)
      datfconv(dnew,d,istart-1,istart-1,jstart,jend,kstart,kend,
               jstep,kstep,jstepn,kstepn,ioff,joff,koff,istep,0);
  
    if (iend != nx-1 && nx != 1)
      datfconv(dnew,d,iend+1,iend+1,jstart,jend,kstart,kend,
               jstep,kstep,jstepn,kstepn,ioff,joff,koff,-istep,0);

    if (jstart != 2 && ny != 1)
      datfconv(dnew,d,istart,iend,jstart-1,jstart-1,kstart,kend,
               jstep,kstep,jstepn,kstepn,ioff,joff,koff,jstep,0);
  
    if (jend != ny-1 && ny != 1)
      datfconv(dnew,d,istart,iend,jend+1,jend+1,kstart,kend,
               jstep,kstep,jstepn,kstepn,ioff,joff,koff,-jstep,0);

    if (kstart != 2 && nz != 1)
      datfconv(dnew,d,istart,iend,jstart,jend,kstart-1,kstart-1,
               jstep,kstep,jstepn,kstepn,ioff,joff,koff,kstep,0);

    if (kend != nz-1 && nz != 1)
      datfconv(dnew,d,istart,iend,jstart,jend,kend+1,kend+1,
               jstep,kstep,jstepn,kstepn,ioff,joff,koff,-kstep,0);

/*    shared edges  */
    istep=1; 
    jstep=iend-istart+3; 
    kstep=jstep*(jend-jstart+3);
    if (nx == 1) { ixd=1; nxd=2;} else { ixd=2; nxd=iend-istart+3;}
    if (ny == 1) { iyd=1; nyd=2;} else { iyd=2; nyd=jend-jstart+3;}
    if (nz == 1) { izd=1; nzd=2;} else { izd=2; nzd=kend-kstart+3;}

    if (nx != 1 && ny != 1)
      { if (istart != 2 || jstart != 2)
          { datfconv(dnew,dnew,1,1,1,1,izd,nzd-1,jstep,kstep,jstep,kstep,1,1,1,istep,jstep); }

        if (istart != 2 || jend != ny-1)
          { datfconv(dnew,dnew,1,1,nyd,nyd,izd,nzd-1,jstep,kstep,jstep,kstep,1,1,1,istep,-jstep); }

        if (iend != nx-1 || jstart != 2)
          { datfconv(dnew,dnew,nxd,nxd,1,1,izd,nzd-1,jstep,kstep,jstep,kstep,1,1,1,-istep,jstep); }

        if (iend != nx-1 || jend != ny-1)
          { datfconv(dnew,dnew,nxd,nxd,nyd,nyd,izd,nzd-1,jstep,kstep,jstep,kstep,1,1,1,-istep,-jstep); } }

    if (nx != 1 && nz != 1)
      { if (istart != 2 || kstart != 2)
          { datfconv(dnew,dnew,1,1,iyd,nyd-1,1,1,jstep,kstep,jstep,kstep,1,1,1,istep,kstep); }

        if (iend != nx-1 || kstart != 2)
          { datfconv(dnew,dnew,nxd,nxd,iyd,nyd-1,1,1,jstep,kstep,jstep,kstep,1,1,1,-istep,kstep); }

        if (istart != 2 || kend != nz-1)
          { datfconv(dnew,dnew,1,1,iyd,nyd-1,nzd,nzd,jstep,kstep,jstep,kstep,1,1,1,istep,-kstep); }

        if (iend != nx-1 || kend != nz-1)
          { datfconv(dnew,dnew,nxd,nxd,iyd,nyd-1,nzd,nzd,jstep,kstep,jstep,kstep,1,1,1,-istep,-kstep); } }

    if (ny != 1 && nz != 1)
      { if (jstart != 2 || kstart != 2)
          { datfconv(dnew,dnew,ixd,nxd-1,1,1,1,1,jstep,kstep,jstep,kstep,1,1,1,jstep,kstep); }

        if (jend != ny-1 || kstart != 2)
          { datfconv(dnew,dnew,ixd,nxd-1,nyd,nyd,1,1,jstep,kstep,jstep,kstep,1,1,1,-jstep,kstep); }

        if (jstart != 2 || kend != nz-1)
          { datfconv(dnew,dnew,ixd,nxd-1,1,1,nzd,nzd,jstep,kstep,jstep,kstep,1,1,1,jstep,-kstep); }

        if (jend != ny-1 || kend != nz-1)
          { datfconv(dnew,dnew,ixd,nxd-1,nyd,nyd,nzd,nzd,jstep,kstep,jstep,kstep,1,1,1,-jstep,-kstep); } }

/*   shared corners */
    if (nx != 1 && ny != 1 && nz != 1)
      { if (istart != 2 || jstart != 2 || kstart != 2)
          { ijkn=0;
            dnew[ijkn]=(dnew[ijkn+istepn]+dnew[ijkn+jstepn]+dnew[ijkn+kstepn])/3.0; }

        if (istart != 2 || jstart != 2 || kend != nz-1)
          { ijkn=(kend-kstart+2)*kstepn;
            dnew[ijkn]=(dnew[ijkn+istepn]+dnew[ijkn+jstepn]+dnew[ijkn-kstepn])/3.0; }

        if (istart != 2 || jend != ny-1 || kstart != 2)
          { ijkn=(jend-jstart+2)*jstepn;
            dnew[ijkn]=(dnew[ijkn+istepn]+dnew[ijkn-jstepn]+dnew[ijkn+kstepn])/3.0; }

        if (istart != 2 || jend != ny-1 || kend != nz-1)
          { ijkn=(jend-jstart+2)*jstepn+(kend-kstart+2)*kstepn;
            dnew[ijkn]=(dnew[ijkn+istepn]+dnew[ijkn-jstepn]+dnew[ijkn-kstepn])/3.0; }

        if (iend != nx-1 || jstart != 2 || kstart != 2)
          { ijkn=iend-istart+2;
            dnew[ijkn]=(dnew[ijkn-istepn]+dnew[ijkn+jstepn]+dnew[ijkn+kstepn])/3.0; }

        if (iend != nx-1 || jstart != 2 || kend != nz-1)
          { ijkn=iend-istart+2+(kend-kstart+2)*kstepn;
            dnew[ijkn]=(dnew[ijkn-istepn]+dnew[ijkn+jstepn]+dnew[ijkn-kstepn])/3.0; }

        if (iend != nx-1 || jend != ny-1 || kstart != 2)
          { ijkn=iend-istart+2+(jend-jstart+2)*jstepn;
            dnew[ijkn]=(dnew[ijkn-istepn]+dnew[ijkn-jstepn]+dnew[ijkn+kstepn])/3.0; }

        if (iend != nx-1 || jend != ny-1 || kend != nz-1)
          { ijkn=iend-istart+2+(jend-jstart+2)*jstepn+(kend-kstart+2)*kstepn;
            dnew[ijkn]=(dnew[ijkn-istepn]+dnew[ijkn-jstepn]+dnew[ijkn-kstepn])/3.0; } }

    if (nx == 1) { itot=1;} else { itot=iend-istart+3;}
    if (ny == 1) { jtot=1;} else { jtot=jend-jstart+3;}
    if (nz == 1) { ktot=1;} else { ktot=kend-kstart+3;}
    
    /* Reallocate */
    dnew = realloc(dnew, itot*jtot*ktot*sizeof(double));
    
    /* Turn xnew into a new PyArray Object */
    npy_intp dims[1] = {itot*jtot*ktot};
    xpnew = PyArray_SimpleNewFromData(1, dims, NPY_DOUBLE, dnew); 

    /* tell numpy to free the data when it's finished with it */
    PyArray_ENABLEFLAGS((PyArrayObject*)xpnew, NPY_ARRAY_OWNDATA);

    return xpnew;
};


static PyMethodDef StreamMethods[] = {
    {"_grdconv", stream_grdconv, METH_VARARGS, "Convert grid data"},
    {"_datconv", stream_datconv, METH_VARARGS, "Convert var data"},
    {NULL, NULL, 0, NULL}       /* Sentinel */
};

/* Module definition structure */
static struct PyModuleDef streamcfd = {
    PyModuleDef_HEAD_INIT,
    "streamcfd",     /* name of module */
    NULL,               /* module documentation */
    -1,                 /* size of per-interpreter state of the module */
    StreamMethods
};

/* Module initialization function */
PyMODINIT_FUNC
PyInit_streamcfd(void)
{   
    /* Initialize numpy arrays */
    import_array();

    return PyModule_Create(&streamcfd);
}

int main (int argc, char *argv[])
{
    wchar_t *program = Py_DecodeLocale(argv[0], NULL);
    if (program == NULL) {
        fprintf(stderr, "Fatal error: cannot decode argv[0]\n");
        exit(1);
    }
    /* Add a built-in module, before Py_Initialize */
    PyImport_AppendInittab("streamcfd", PyInit_streamcfd);

    /* Pass argv[0] to the Python interpreter */
    Py_SetProgramName(program);

    /* Initialize the Python interpreter. Required */
    Py_Initialize();

    PyMem_RawFree(program);
    return 0;
}